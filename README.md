# Seccomp Filters from Fuzzing

This project provides the source code for automatically generating seccomp filters with the help of AFL++. Furthermore, a Vagrantfile is provided to help setting up the project dependencies. 

## Licensing Terms 

The source code in this project is released under the terms of the GNU General Public License v2. 

## Prerequisites 
Change the system resources used for the virtual machine. Currently they are set to the following: 

```
vb.cpus = 6
vb.memory = "11264"
```

Afterwards the vagrant box can be build. 

## Binary Compilation 

Source code that should be analysed with this tool, has to be compiled with debugging information and according AFL instrumentation. Furthermore, the bit code has to be extracted to work with symbolic execution in the beginning. 

### KLEE Resources 
The LLVM Wrapper `wllvm` has to be instructed to use LLVM version 9, since KLEE relies on specific DWARF versions: 
```bash 
export LLVM_COMPILER=clang
export LLVM_COMPILER_PATH=/lib/llvm-9/bin
export LLVM_CC_NAME=clang-9
```
Binaries should be then compiled with: 
```bash
wllvm -g -O1 -Xclang -disable-llvm-passes -D__NO_STRING_INLINES  -D_FORTIFY_SOURCE=0 -U__OPTIMIZE__ <path_to_bin> 
``` 

Afterwards the bit code has to be extracted:
```bash
extract-bc -l llvm-link-9 <path_to_bin>
```

### AFL Resources
Compile the sources once again with AFL specific instrumentation: 
```bash 
afl-clang fast <path_to_bin>
```


## Execution 

Before the main script can be executed, the configuration of the Custom Mutator has to be manually changed in order to only invoke existent files/folders. Create a folder with example files for the SUT to work with and then paste the absolute path in `afl_custom_mutators\weighted_input_mutator_experimental\rootlocations.txt`. 
Change the current working directory to `seccomp_fuzzing_src` and the tool can be executed: 

```bash
python3 ./policiesGenerator.py --bin_path <normal bin path> --duration <duration> -n <normal bin path> -bc <KLEE bit code> -afl <AFL instructed bin> 
```